from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve, reverse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .views import home, signup
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class IndexTestCase(TestCase):

    def test_home_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_home_use_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home.html')

    def test_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)


class SignUpTestCase(TestCase):

    def test_signup_url_is_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code,200)

    def test_signup_use_index_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_signup_func(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup)

class FormTestCase(TestCase):

    def test_singup_form(self):

        response = Client().post(reverse('homepage:signup'), data = {'username': 'Lindaine123', 'password1' : 'aihysbdiabda234', 'password2' : 'aihysbdiabda234'})
        count = User.objects.all().count()
        input = User.objects.all().first()
        self.assertEqual(input.username, 'Lindaine123')
        self.assertEqual(count,1)